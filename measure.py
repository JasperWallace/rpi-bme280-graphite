#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import print_function
import argparse
from graphiteclient import GraphiteClient

# https://github.com/adafruit/Adafruit_Python_BME280

from Adafruit_BME280 import *

server = port = None

parser = argparse.ArgumentParser(description='send temp and pressure and humidity stats to graphite.')

parser.add_argument('--server', type=str,
  help='The graphite server hostname')

parser.add_argument('--port',
  type=int,
  help='The graphite server port (defaults to 2003)')

# it finds the bus automaticly?

def check_addr(arg):
  addr = arg
  try:
    if addr.startswith("0x"):
      addr = int(addr, 16)
    else:
      addr = int(addr)
  except Exception as e:
    raise argparse.ArgumentTypeError("addr should be an int, not %s" % (arg, e))
  return addr

parser.add_argument('--address',
  type=check_addr,
  help='The i2c address to use')

args = parser.parse_args()

if args.server:
  server = args.server

if args.port:
  port = args.port

if server:
  client = GraphiteClient(server, port)
else:
  client = GraphiteClient()

client.verbose = True

if args.address:
  bme = BME280(t_mode=BME280_OSAMPLE_8, p_mode=BME280_OSAMPLE_8, h_mode=BME280_OSAMPLE_8, address=args.address)
else:
  bme = BME280(t_mode=BME280_OSAMPLE_8, p_mode=BME280_OSAMPLE_8, h_mode=BME280_OSAMPLE_8)

while True:
  degrees = bme.read_temperature()
  pascals = bme.read_pressure()
  hectopascals = pascals / 100
  humidity = bme.read_humidity()

  client.poke("environment.temperature.bme280.%s", degrees)
  client.poke("environment.humidity.bme280.%s", humidity)
  client.poke("environment.pressure.bme280.%s", hectopascals)
  time.sleep(15)

print("exiting")
