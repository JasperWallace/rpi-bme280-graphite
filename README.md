# Get temp/humidity/pressure from a bme280 and send to graphite

needs:

https://github.com/adafruit/Adafruit_Python_GPIO.git
https://github.com/adafruit/Adafruit_Python_BME280.git

The BME/BMP280 breakout board I have uses addr 0x76, but the Adafruit python module defaults to 0x77
